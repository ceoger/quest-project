import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import * as user from '@/views/modules/user.js'
import * as notification from '@/views/modules/notification.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    notification
  },
  state: {
    user: null,
    student: null
  },
  mutations: {
    SET_USER_DATA (state, userData) {
      state.user = userData
      localStorage.setItem('user', JSON.stringify(userData))
      axios.defaults.headers.common['Authorization'] = `Bearer ${
        userData.token
      }`
    },
    CLEAR_USER_DATA(){
      // state.user = null
      localStorage.removeItem('user')
      location.reload()
      // axios.defaults.headers.common['Authorization'] = null
    },
    SET_STUDENT_DATA(state, studentData){
      state.student = studentData
    }
    // GET_DATA(state, userData){
    //   state.user.push = getData
    // }
  },
  actions: {
    register ({ commit }, credentials) {
      return axios
        .post('http://localhost:3000/users', credentials)
        .then(({ data }) => {
          commit('CLEAR_USER_DATA', data)
          // console.log('user data is:', data );
        })
    },
    registration ({ commit }, credentials) {
      return axios
        .post('http://localhost:3000/students', credentials)
        .then(({ data }) => {
          commit('SET_USER_DATA', data)
          // console.log('user data is:', data );
        })
    },
    login ({ commit, dispatch }, credentials) {
      return axios
        .post('http://localhost:3000/auth/login', credentials)
        .then(({ data }) => {
          commit('SET_USER_DATA', data)
          this.$router.push({name: 'login'})
        })
        .catch(error => {
        const notification = {
          type: 'error',
          message: 'There was a problem logging in: ' + error.message
        }
        dispatch('notification/add', notification, { root: true })
      })
    },
    fetchData ({ commit, dispatch }) {
      return axios
        .get('http://localhost:3000/users/:_username')
        .then(response => {
          commit('SET_USER_DATA', response.data)
          // console.log('user data is:', data );
        })
        .catch(error => {
        const notification = {
          type: 'error',
          message: 'There was a problem fetching events: ' + error.message
        }
        dispatch('notification/add', notification, { root: true })
      })
    },
    // checkLoggInStatus ({ commit }, credentials) {
    //   return axios
    //     .get('http://localhost:3000/users/:_username', credentials)
    //     .then(({ data }) => {
    //       commit('SET_USER_DATA', data)
    //     })
    //     .catch(error => {
    //       console.log("check login error", error);
    //     })
    // },
    logout({commit}){
      // return axios
      // .delete('http://localhost:3000/users/: _username')
      // .then(({data}) =>{
        commit('CLEAR_USER_DATA')
      // })
    }
  },
  getters: {
    loggedIn (state) {
      return !!state.user
    // },
    // logOut (state) {
    //   return !!state.user
    }
  }
})
