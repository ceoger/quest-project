import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

const Student = {
  template: `<div>Student{{ $route.params.id }}</div>`
}

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Dashboard.vue'),
      meta: { requiresAuth: true }
      // beforeEnter (to, from, next) {
      // if (!loggedIn()) {
      // return next({
      // name: 'login'
      // })
      // }
      // next()
      // }
    },
    {
      path: '/register',
      name: 'register',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/RegisterUser.vue')
    },
    {
      path: '/student-registration',
      name: 'student-registration',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/StudentReg.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/student/:id',
      name: 'student',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/StudentProfile.vue'),
      meta: { requiresAuth: true },
      props: true
    },
    {
      path: '/student/pro',
      name: 'student-pro',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/StudentPro.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/LoginUser.vue')
    },
    // {
    //   path: '/dashboard',
    //   name: 'dashboard',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/Dashboard.vue')
    // }

  ]
})
 router.beforeEach((to, from, next) => {
   const loggedIn = localStorage.getItem('user')
    if(to.matched.some(record => record.meta.requiresAuth) && !loggedIn){
        next('/')
      }
    next()
 })
export default router
